public class Schuettbunker {

    private final int maxContent;
    private int[] silos;
    private int position;

    Schuettbunker(int size,int maxContent) {
        this.silos = new int[size];
        this.maxContent = maxContent;
        this.position=0;
    }

    void nachLinks(int i) {

        if (this.position - i < 0) this.position = 0;
        else if(i>0) this.position -= i;
    }

    void nachRechts(int i) {

        if (this.position + i >= silos.length) this.position = silos.length - 1;
        else if(i>0) this.position += i;
    }

    void fuell(int i) {
        if (this.silos[position] + i > maxContent) silos[position] = maxContent;
        else if(i>0) silos[position] += i;
    }

    int gibAlles() {
        int content = silos[position];
        silos[position] = 0;
        return content;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < silos.length; i++) {
            stringBuilder.append(i+1).append(": ").append(silos[i]).append("\n");
        }
        stringBuilder.append("Kran: ").append(position+1).append("\n");
        return stringBuilder.toString();
    }

    public static void main(String []args) {

        Schuettbunker sb = new Schuettbunker(5,10);
        // 5 Silos, Maximalwert 10

        sb.fuell(4);
        sb.fuell(1);
        sb.nachRechts(2); // Kran 2 Positionen nach rechts
        sb.fuell(20);
        //Maximalwert 10
        sb.nachRechts(2); // Kran 2 Positionen nach rechts
        sb.fuell(1);
        sb.nachLinks(42); // ganz nach Links, keine Randueberschreitung
        int temp = sb.gibAlles();
        sb.nachRechts(1);
        sb.fuell(temp);

        System.out.println(sb);
    }

}
