import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AsciiTester {

    public static void main(String[] args) throws FileNotFoundException {

        String line;
        int lines=0;
        int rows;

        if (args.length!=1) {
            System.out.println("Usage: ... <filename>");
            System.exit(1);
        }

        Scanner file = new Scanner(new File(args[0]));

        try (file) {
            if (!file.hasNext()) {
                System.out.println("Empty file!");
                System.exit(1);
            }


            lines++;
            line = file.nextLine();
            rows = line.length();

            while (file.hasNextLine()) {
                lines++;
                line = file.nextLine();
                if (rows != line.length())
                    throw new Exception("Line " + lines + " has too many rows");

            }

            System.out.printf("ok %d %d", rows, lines);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

}
