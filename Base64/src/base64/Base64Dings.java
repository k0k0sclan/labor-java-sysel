package base64;

import java.util.Scanner;

public class Base64Dings {

    private char[] conversionTable;

    public Base64Dings() {
        conversionTable = new char[64];

        for (int i = 0; i < 26; i++) {
            conversionTable[i] = (char) ('A' + i);
        }
        for (int i = 26; i < 52; i++) {
            conversionTable[i] = (char) ('a' + i - 26);
        }
        for (int i = 52; i < 62; i++) {
            conversionTable[i] = (char) ('0' + i - 52);
        }
        conversionTable[62] = '+';
        conversionTable[63] = '/';
    }


    public String encode(String input) {

        StringBuilder output = new StringBuilder();
        StringBuilder rawBits = new StringBuilder();
        String rawBitsFinal;

        for (int i = 0; i < input.length(); i++) {
            StringBuilder binaryString = new StringBuilder(Integer.toBinaryString(input.charAt(i)));
            int paddingSize = (8 - (binaryString.length() % 8)) % 8;
            for (int j = 0; j < paddingSize; j++) {
                binaryString.insert(0, "0");
            }
            rawBits.append(binaryString);
        }

        int paddingSize = (6 - (rawBits.length() % 6)) % 6;
        for (int i = 0; i < paddingSize; i++) {
            rawBits.append("0");
        }
        rawBitsFinal = rawBits.toString();

        for (int i = 0; i < rawBitsFinal.length() / 6; i++) {
            int index = Integer.parseInt(rawBitsFinal.substring(i * 6, (i + 1) * 6), 2);
            output.append(conversionTable[index]);
        }

        paddingSize = (4 - (output.length() % 4)) % 4;

        for (int i = 0; i < paddingSize; i++) {
            output.append("=");
        }

        return output.toString();
    }

    public static void main(String[] args) {

        Base64Dings dings = new Base64Dings();
        Scanner scanner = new Scanner(System.in);

        System.out.println("String");
        String out = scanner.nextLine();
        System.out.println("Anzahl");


        for (int i = scanner.nextInt(); i > 0; i--) {
            out = dings.encode(out);
        }
        System.out.println(out);

    }

}
