package rekursion;

import org.junit.Test;

import static org.junit.Assert.*;

public class RekursionTest {

    @Test
    public void testZahlenfolge() {

        assertEquals(13.0/15.0,Zahlenfolge.zahlenfolge_it(3),1E-6);
        assertEquals(0.0,Zahlenfolge.zahlenfolge_it(-4),1E-6);
        assertEquals(13.0/15.0,Zahlenfolge.zahlenfolge_rek(3),1E-6);
        assertEquals(0.0,Zahlenfolge.zahlenfolge_rek(-4),1E-6);
        assertEquals(Zahlenfolge.zahlenfolge_rek(7),Zahlenfolge.zahlenfolge_it(7),1E-6);

    }

    @Test
    public void testFibonacci() {

        assertEquals(21,Fibonacci.rekursiv(8));
        assertEquals(1,Fibonacci.rekursiv(1));
        assertEquals(0,Fibonacci.rekursiv(-5));
        assertEquals(21,Fibonacci.iterativ(8));
        assertEquals(1,Fibonacci.iterativ(1));
        assertEquals(0,Fibonacci.iterativ(-5));
        assertEquals(Fibonacci.rekursiv(24),Fibonacci.iterativ(24));

    }

    @Test
    public void testGGT() {

        assertEquals(7,GGT.ggt(21,7));
        assertEquals(7,GGT.ggt(49,14));
    }

    @Test
    public void testGGTException() {

        try {
            GGT.ggt(-4,14);
            fail("Exception 1 not thrown");
        } catch (ArithmeticException e) {
            System.out.println("Exception 1 thrown");
        }
        try {
            GGT.ggt(0,0);
            fail("Exception 2 not thrown");
        } catch (ArithmeticException e) {
            System.out.println("Exception 2 thrown");
        }

    }

    @Test
    public void testPascalTriangle() {

        assertEquals(1,PascalTriangle.binomialkoeffizient(1,1));
        assertEquals(1,PascalTriangle.binomialkoeffizient(200,0));
        assertEquals(6,PascalTriangle.binomialkoeffizient(4,2));
        assertEquals(0,PascalTriangle.binomialkoeffizient(-9,3));
        assertEquals(1,PascalTriangle.binomialkoeffizient(0,0));

    }

}