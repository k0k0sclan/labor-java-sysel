package rekursion;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            System.out.println("Zahlenfolge "+i+": "+ Zahlenfolge.zahlenfolge_it(i));
        }

        for (int i = 0; i < 20; i++) {
            System.out.println("Fibonacci "+i+": "+ Fibonacci.iterativ(i));
        }

        System.out.println("GGT.ggt(21,25) = " + GGT.ggt(21, 56));

        System.out.println("PascalTriangle.binomialkoeffizient(5,3) = " + PascalTriangle.binomialkoeffizient(5, 3));

    }
}
