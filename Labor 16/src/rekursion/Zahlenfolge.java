package rekursion;

public class Zahlenfolge {

    static double zahlenfolge_rek(int i) {
        if (i <= 0) {
            return 0;
        }
        return i % 2 == 1 ? 1.0 / (2 * i - 1) + zahlenfolge_rek(i - 1) : zahlenfolge_rek(i - 1) - 1.0 / (2 * i - 1);
    }

    static double zahlenfolge_it(int i) {
        double sum = 0;

        for (int j = i; j > 0; j--) {
            if (j % 2 == 1) sum += 1.0 / (2 * j - 1);
            else sum -= 1.0 / (2 * j - 1);
        }

        return sum;
    }
}
