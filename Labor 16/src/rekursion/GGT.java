package rekursion;

public class GGT {

    static int ggt(int x, int y) {
        if(x==0&&y==0) throw new ArithmeticException("GGT von 0");
        if(x<0||y<0) throw new ArithmeticException("GGT von negativen Zahlen");
        if (y==0) return x;
        else return ggt(y, x % y);
    }



}
