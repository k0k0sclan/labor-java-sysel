package rekursion;

public class Fibonacci {

    static int rekursiv(int index) {
        return index > 2 ? rekursiv(index-1) + rekursiv(index - 2) : (index > 0 ? 1 : 0);
    }

    static int iterativ(int index) {

        if (index<1) return 0;
        if (index<=2) return 1;

        int previous=1;
        int current=1;
        int next;

        for (int i = 2; i < index; i++) {
            next = previous + current;
            previous = current;
            current = next;
        }

        return current;
    }

}
