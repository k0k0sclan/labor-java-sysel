package rekursion;

public class PascalTriangle {

    static int binomialkoeffizient(int n, int k) {

        if(k<0||n<0||k>n) return 0;
        if (n == k || k == 0) return 1;
        else return binomialkoeffizient(n - 1, k) + binomialkoeffizient(n - 1, k - 1);

    }


}
