public class Person implements Speaker {
    private boolean stumm;

    public Person() {
        if (Math.random()<0.05) stumm = true;
    }

    public String speak() throws DumbException {
        if(stumm) throw new DumbException("Person ist stumm \uD83D\uDE4A");
        return "Hallo";
    }
}
