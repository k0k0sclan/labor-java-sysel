public class Main {
    public static void main(String[] args) {

        Speaker[] arr = new Speaker[100];

        for (int i = 0; i < arr.length; i++) {
            double rand = Math.random()*3;
            if (rand < 1) arr[i] = new Person();
            else if (rand < 2) arr[i] = new Dog();
            else if (rand < 3) arr[i] = new Cat();
        }

        for (Speaker s :
                arr) {
            try {
                System.out.println(s.speak());
            } catch (DumbException e) {
                System.out.println("Fehler! " + e.getMessage());
            }
        }

        int sum = 0;
        for (Speaker speaker: arr) {
            if(speaker instanceof Person) sum++;
        }
        System.out.println("sum = " + sum);



    }
}
