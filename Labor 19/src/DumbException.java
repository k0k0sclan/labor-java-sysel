public class DumbException extends Exception {

    public DumbException() {
        super();
    }

    public DumbException(String message) {
        super(message);
    }
}
