public interface Speaker {
    String speak() throws DumbException;
}
