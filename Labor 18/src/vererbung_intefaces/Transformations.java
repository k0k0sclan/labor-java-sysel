package vererbung_intefaces;

public interface Transformations {

    void translate(double x, double y);

    void scale(double a);

}
