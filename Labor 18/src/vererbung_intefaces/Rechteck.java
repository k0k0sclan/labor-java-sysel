package vererbung_intefaces;

public class Rechteck extends Figur {

    private double height;
    private double width;

    @Override
    public void scale(double a) {
        if (a <= 0) throw new IllegalArgumentException("Kann nicht negativ Skalieren");
        height *= a;
        width *= a;
    }

    public Rechteck() {
    }

    public Rechteck(double x, double y, double height, double width) {
        super(x, y);
        if (height <= 0 || width <= 0) throw new IllegalArgumentException("Negative Laenge");
        this.height = height;
        this.width = width;
    }

    @Override
    public String toString() {
        return "Rechteck(" + getX() + "/" + getY() + ';' + height + ";" + width + ")";
    }
}
