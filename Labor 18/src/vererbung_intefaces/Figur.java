package vererbung_intefaces;

public abstract class Figur implements Transformations {

    private double x;
    private double y;

    public Figur(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Figur() {
    }

    @Override
    public void translate(double x, double y) {
        this.x += x;
        this.y += y;
    }

    @Override
    public String toString() {
        return "Figur(" + x + "/" + y + ')';
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}