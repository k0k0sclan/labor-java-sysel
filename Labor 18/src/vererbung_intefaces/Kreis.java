package vererbung_intefaces;

public class Kreis extends Figur {

    private double r;

    public Kreis(double x, double y, double r) {
        super(x, y);
        if(r<=0) throw new IllegalArgumentException("Negativer Radius");
        this.r = r;
    }

    public Kreis() {
        this(1);
    }

    public Kreis(double r) {
        this(0, 0, r);
    }

    @Override
    public void scale(double a) {
        if(a<=0) throw new IllegalArgumentException("Negative skalierung!");
        r *= a;
    }

    @Override
    public String toString() {
        return "Kreis(" + getX() + "/" + getY() + ';' + r + ")";
    }
}
