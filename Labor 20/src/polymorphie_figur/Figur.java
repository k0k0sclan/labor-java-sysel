package polymorphie_figur;

public abstract class Figur {

    public abstract double flaeche();
    public abstract double umfang();

}
