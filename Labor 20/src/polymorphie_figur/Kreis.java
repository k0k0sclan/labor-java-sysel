package polymorphie_figur;

public class Kreis extends Figur {

    private double radius;

    public Kreis(double radius) {
        if (radius <= 0) throw new IllegalArgumentException("Values must not be negative");
        this.radius = radius;
    }

    public Kreis(double minRadius, double maxRadius) {
        if (minRadius <= 0 || maxRadius < minRadius)
            throw new IllegalArgumentException("Values must not be negative");
        this.radius = Math.random() * (maxRadius - minRadius) + minRadius;
    }

    @Override
    public double flaeche() {
        return radius * radius * Math.PI;
    }

    @Override
    public double umfang() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return "Kreis{" +
                "radius: " + radius +
                '}';
    }
}
