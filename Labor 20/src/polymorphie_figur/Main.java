package polymorphie_figur;

public class Main {
    public static void main(String[] args) {

        Figur[] arr = new Figur[20];
        double uSum = 0;
        double fSum = 0;

        for (int i = 0; i < arr.length; i++) {
            if (Math.random() < 0.5) arr[i] = new Kreis(1, 10);
            else arr[i] = new Rechteck(1, 1, 10, 10);
        }

        for (Figur figur :
                arr) {
            System.out.println(figur);
            uSum += figur.umfang();
            fSum += figur.flaeche();
        }

        System.out.println("fSum = " + fSum);
        System.out.println("uSum = " + uSum);

    }
}
