package polymorphie_figur;

public class Rechteck extends Figur {

    private double laenge;
    private double breite;

    public Rechteck(double laenge, double breite) {
        if (laenge < 0 || breite < 0) throw new IllegalArgumentException("Values must not be negative");

        this.laenge = laenge;
        this.breite = breite;
    }
    public Rechteck(double minLaenge, double minBreite,double maxLaenge, double maxBreite) {
        if (minLaenge < 0 || minBreite < 0 || maxBreite < minBreite || maxLaenge < minBreite ) throw new IllegalArgumentException("Values must not be negative");
        this.laenge = Math.random()*(maxLaenge-minLaenge)+minLaenge;
        this.breite = Math.random()*(maxBreite-minBreite)+minBreite;
    }

    @Override
    public double flaeche() {
        return laenge*breite;
    }

    @Override
    public double umfang() {
        return 2*(laenge+breite);
    }

    @Override
    public String toString() {
        return "Rechteck{" +
                "laenge: " + laenge +
                ", breite: " + breite +
                '}';
    }
}
