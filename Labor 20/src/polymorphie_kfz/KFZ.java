package polymorphie_kfz;

import java.util.Arrays;
import java.util.Random;

public abstract class KFZ {
    public static String[] automarken = {"BMW", "Honda", "Suzuki", "VW", "Ferrari", "Alfa Romeo", "Mercedes", "Maserati"};
    public static String[] motomarken = {"BMW", "Honda", "Suzuki", "Ducati", "Moto Guzzi", "Kawasaki"};
    private String marke;

    public boolean isMotorLaeuft() {
        return motorLaeuft;
    }

    public void setMotorLaeuft(boolean motorLaeuft) {
        this.motorLaeuft = motorLaeuft;
    }

    private boolean motorLaeuft;

    // Notwendige Konstruktoren

    private static Random rd = new Random();

    public KFZ(String marke) {
        this.marke = marke;
    }

    // Notwendige Methoden

    public static KFZ createKFZ(boolean isMotorrad, String marke) {
        if (isMotorrad) {
            if (Arrays.stream(motomarken).noneMatch(s -> s.equals(marke)))
                throw new IllegalArgumentException("Motorradmarke " + marke + " existiert nicht!");

            return new Motorrad(marke);
        } else {
            if (Arrays.stream(automarken).noneMatch(s -> s.equals(marke)))
                throw new IllegalArgumentException("Automarke " + marke + " existiert nicht!");

            return new Auto(marke);
        }
    }

    public void starten() {
        motorLaeuft = true;
    }

    // Gibt Informationen aus
    public abstract void info();

    public static void main(String[] args) {

        KFZ[] f = new KFZ[30];

        for (int i = 0; i < f.length; i++) {
            if (rd.nextBoolean()) {
                f[i] = createKFZ(true, motomarken[rd.nextInt(motomarken.length)]);
            } else {
                f[i] = createKFZ(false, automarken[rd.nextInt(automarken.length)]);
            }
        }

        for (int i = 0; i < f.length; i++) {
            if (rd.nextBoolean()) {
                f[i].starten();
            }
        }

        for (KFZ kfz : f) {
            if ((kfz instanceof Auto && kfz.motorLaeuft) || (kfz instanceof Motorrad && !kfz.motorLaeuft)) {
                kfz.info();
            }
        }

    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }
}
