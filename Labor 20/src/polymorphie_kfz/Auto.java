package polymorphie_kfz;

public class Auto extends KFZ {
    public Auto(String marke) {
        super(marke);
    }

    @Override
    public void info() {
        System.out.println("Auto "+ getMarke() + (isMotorLaeuft()?" ist gestartet":" ist nicht gestartet"));
    }
}
