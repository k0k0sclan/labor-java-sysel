package polymorphie_kfz;

public class Motorrad extends KFZ {

    public Motorrad(String marke) {
        super(marke);
    }
    @Override
    public void info() {
        System.out.println("Motorrad "+ getMarke() + (isMotorLaeuft()?" ist gestartet":" ist nicht gestartet"));
    }

}
