package rectangleklasse;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

    Rectangle instance = new Rectangle(5.0, 3.0);

    @Test
    void setLength1() {
        instance.setLength(33);
        assertEquals(instance.getLength(), 20.0);

    }

    @Test
    void setLength2() {
        instance.setLength(19.0);
        assertEquals(instance.getLength(), 19.0);
    }
    
    @Test
    void setLength3() {
        instance.setLength(-2.0);
        assertEquals(instance.getLength(), 1.0);
    }

    @Test
    void setWidth1() {
        instance.setWidth(33);
        assertEquals(instance.getWidth(), 20.0);
    }

    @Test
    void setWidth2() {
        instance.setWidth(19.0);
        assertEquals(instance.getWidth(), 19.0);
    }

    @Test
    void setWidth3() {
        instance.setWidth(-2.0);
        assertEquals(instance.getWidth(), 1.0);
    }
    

    @Test
    void getPerimeter() {
        assertEquals(instance.getPerimeter(), 16,10E-6);
    }

    @Test
    void getArea() {
        assertEquals(instance.getArea(), 15,10E-6);
    }
}