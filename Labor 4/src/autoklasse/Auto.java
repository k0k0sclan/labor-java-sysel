package autoklasse;

public class Auto {

    private String typ;
    private boolean motorAn;

    public static void main(String[] args) {
        Auto a1 = new Auto("BMW");
        // BMW, Motor aus
        Auto a2 = new Auto("Porsche", true);
        // Porsche, Motor an
        System.out.println(a1);
        //Ausgabe: BMW, Motor aus
        a1.starten();
        System.out.println(a1);
        //Ausgabe: BMW, Motor an
        System.out.println(a2);
        //Ausgabe: Porsche, Motor an
        a2.abstellen();
        System.out.println(a2);
        //Ausgabe: Porsche, Motor aus
    }

    private void abstellen() {
        this.motorAn = false;
    }

    private void starten() {
        this.motorAn = true;
    }

    private Auto(String typ, boolean motorAn) {
        this.typ = typ;
        this.motorAn = motorAn;
    }

    private Auto(String typ) {
        this(typ, false);
    }

    @Override
    public String toString() {
        return this.typ + " Motor an: " + this.motorAn;
    }
}
