package personenklasse;

import java.time.LocalDate;

@SuppressWarnings("WeakerAccess")
public class Person {

    private String vorname;
    private String zuname;
    private int geburtsjahr;


    public static void main(String[] args) {
        Person p = new Person();        // name = vorname = "N.N.", geb = 0
        Person p1 = new Person(1978);   // name = vorname = "N.N."
        Person p2 = new Person("Maier", 1967);  // vorname = "N.N."
        Person p3 = new Person("Schmidt", "Franz", 1967);
        p1.setZuname("Bauer");
        p1.setVorname("Hans");
        p2.setVorname("Eva");
        System.out.println(p1 + " ist " + p1.getAlter() +
                " Jahre alt.");
        if (p2.gleichAlt(p3)) {
// Instanzmethode gleichAlt()
            System.out.println(p2 + " und " + p3 +
                    " sind gleich alt.");
        }
        if (Person.gleichAlt(p2, p3)) { // Klassenmethode gleichAlt()
            System.out.println(p2 + " und " + p3 +
                    " sind gleich alt.");
        }
    }


    Person(String vorname, String zuname, int geburtsjahr) {
        this.vorname = vorname;
        this.zuname = zuname;
        this.geburtsjahr = geburtsjahr;
    }

    Person(String zuname, int geburtsjahr) {
        this("N.N.",zuname, geburtsjahr);
    }

    Person(int geburtsjahr) {
        this("N.N.", geburtsjahr);
    }

    Person() {
        this(0);
    }


    public int getAlter() {
        int year = LocalDate.now().getYear();
        return year - this.getGeburtsjahr();
    }


    public boolean gleichAlt(Person person) {
        return this.getAlter() == person.getAlter();
    }

    public static boolean gleichAlt(Person person1, Person person2) {
        return person1.gleichAlt(person2);
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getZuname() {
        return zuname;
    }

    public void setZuname(String zuname) {
        this.zuname = zuname;
    }

    public int getGeburtsjahr() {
        return geburtsjahr;
    }

    @Override
    public String toString() {
        return this.getVorname() + " " + this.getZuname();
    }
}