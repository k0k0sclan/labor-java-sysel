package rectangleklasse;

public class Rectangle {

    private double length;
    private double width;

    Rectangle(double length, double width) {
        this.setLength(length);
        this.setWidth(width);
    }

    Rectangle() {
        this(1.0, 1.0);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        if (length < 1.0) this.length = 1.0;
        else if (length > 20.0) this.length = 20.0;
        else this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        if (width < 1.0) this.width = 1.0;
        else if (width > 20.0) this.width = 20.0;
        else this.width = width;
    }


    public double getPerimeter() {
        return this.getLength() * 2 + this.getWidth() * 2;
    }

    public double getArea() {
        return this.getLength() * this.getWidth();
    }

}
