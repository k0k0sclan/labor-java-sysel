package medium;

public class Buch extends Medium {
    private int seitenAnzahl;

    public Buch(String bezeichnung, double preis, int seitenAnzahl) {
        super(bezeichnung, preis);
        this.seitenAnzahl = seitenAnzahl;
    }

    public int getSeitenAnzahl() {
        return seitenAnzahl;
    }

    public void setSeitenAnzahl(int seitenAnzahl) {
        this.seitenAnzahl = seitenAnzahl;
    }

    @Override
    public void info() {
        System.out.printf("-----------------------------------------------\n" +
                "Buch:\t%s\n" +
                "Preis:\t%.2f\n" +
                "Seiten:\t%d\n",getBezeichnung(),getPreis(),getSeitenAnzahl());
    }

}
