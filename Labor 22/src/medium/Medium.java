package medium;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public abstract class Medium implements Comparable {

    public static final String PATHNAME = "C:\\Users\\Peter Sysel\\Programmieren\\laborübungen\\Labor 22\\src\\medium\\Mediumliste.csv";

    public String getBezeichnung() {
        return bezeichnung;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Medium)) throw new IllegalArgumentException("Object is not of type Medium");
        return compareTo((Medium) o);
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    private String bezeichnung;
    private double preis;

    public Medium(String bezeichnung, double preis) {
        this.bezeichnung = bezeichnung;
        this.preis = preis;
    }

    public abstract void info(); // Gibt eine Beschreibung auf System.out aus

    public int compareTo(Medium other) {
        return Double.compare(this.preis, other.preis);
    }


    public static void main(String[] args) {
        // Legen Sie ein Array m  der Groesse 4
        // vom Typ Medium an und weisen Sie
        // je zwei Buch und zwei AudioCD - Instanzen an
        // Rufen Sie fuer jedes Element im Array m
        // die Methode info() auf }

        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(PATHNAME));
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        ArrayList<String> list = new ArrayList<>();

        while (scanner.hasNextLine()) {
            list.add(scanner.nextLine());
        }

        ArrayList<Medium> mediumArrayList = new ArrayList<>();

        for (String s : list) {
            String[] arr = s.split(";");
            if (arr.length != 3) throw new IllegalArgumentException("Wrong file format");
            try {
                if (arr[2].contains(".")) {
                    mediumArrayList.add(new AudioCD(arr[0], Double.parseDouble(arr[1]), Double.parseDouble(arr[2])));
                } else {
                    mediumArrayList.add(new Buch(arr[0], Double.parseDouble(arr[1]), Integer.parseInt(arr[2])));
                }
            } catch (NumberFormatException e) {
                System.err.println("Wrong number format in file: " + s);
            }
        }

        Medium[] m = mediumArrayList.toArray(new Medium[0]);

        for (Medium medium : m) {
            medium.info();
        }

        System.out.println("\n\n\nSortieren\n\n\n");
        Arrays.sort(m);

        for (Medium medium : m) {
            medium.info();
        }

    }
}