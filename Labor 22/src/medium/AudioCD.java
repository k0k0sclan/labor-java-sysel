package medium;

public class AudioCD extends Medium {
    private double spielzeit;

    public AudioCD(String bezeichnung, double preis, double spielzeit) {
        super(bezeichnung, preis);
        this.spielzeit = spielzeit;
    }

    public double getSpielzeit() {
        return spielzeit;
    }

    public void setSpielzeit(int spielzeit) {
        this.spielzeit = spielzeit;
    }

    @Override
    public void info() {
        System.out.printf("-----------------------------------------------\n" +
                "AudioCD:\t%s\n" +
                "Preis:\t%.2f\n" +
                "Spielzeit:\t%.1f\n", getBezeichnung(), getPreis(), getSpielzeit());
    }

}
