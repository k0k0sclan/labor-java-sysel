package figurVererbung;

public class Quadrat extends Figur2D {

    private double seite;

    public Quadrat(double seite) throws NegativException {
        super();
        if(seite < 0) throw new NegativException("Negativer Wert!");
        this.seite = seite;
    }

    @Override
    double umfang() {
        return 4*seite;
    }

    @Override
    double flaeche() {
        return seite*seite;
    }

    @Override
    void info() {
        System.out.printf("Quadrat mit Seite %.1f\n",seite);
    }
}
