package figurVererbung;

public class Kreis extends Figur2D {

    private double radius;

    public Kreis(double radius) throws NegativException {
        super();
        if(radius < 0) throw new NegativException("Negativer Wert!");
        this.radius = radius;
    }

    @Override
    double umfang() {
        return 2*Math.PI*radius;
    }

    @Override
    double flaeche() {
        return Math.PI*radius*radius;
    }

    @Override
    void info() {
        System.out.printf("Kreis mit Radius %.1f\n", radius);
    }
}
