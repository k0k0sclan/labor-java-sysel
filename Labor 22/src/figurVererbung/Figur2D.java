package figurVererbung;

public abstract class Figur2D {

    abstract double umfang();
    abstract double flaeche();
    abstract void info();

    static Figur2D figurFactory(char type, double parameter) throws NegativException {
        switch (type) {
            case 'q':
                return new Quadrat(parameter);
            case 'k':
                return new Kreis(parameter);
            default:
                throw new IllegalArgumentException("Illegal Figur type");
        }
    }

    public static void main(String[] args) {

        char []art = {'k', 'q', 'q', 'q', 'k', 'q', 'k', 'k', 'q', 'q'};
        double []len =  {3.8, 3.6, 2.5, 1.2, 4.5, 3.7, 4.5, 5.2, 3.8, 1.5};
        double []len2 =  {3.8, -3.6, 2.5, -1.2, 4.5, 3.7, -4.5, 5.2, 3.8, 1.5};
        Figur2D[] arr = new Figur2D[10];
        double sum = 0;

        for (int i = 0; i < 10; i++) {
            try {
                arr[i] = figurFactory(art[i], len2[i]);
            } catch (NegativException e) {
                System.err.println("Figur " + (i+1) + " fehler: Wert negativ!");
            }
        }

        for (Figur2D figur :
                arr) {
            if (figur == null) {
                System.err.println("Figur ist null");
                continue;
            }
            figur.info();
            if(figur instanceof Quadrat) sum += figur.umfang();
            else if(figur instanceof Kreis) sum += figur.flaeche();
        }
        System.out.println("sum = " + sum);

    }

}
