package figurVererbung;

public class NegativException extends Exception {
    public NegativException(String s) {
        super(s);
    }
}
