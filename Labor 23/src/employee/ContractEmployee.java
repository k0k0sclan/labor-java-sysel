package employee;

import java.text.NumberFormat;

public class ContractEmployee extends Employee {

    private double dailyRate;


    public ContractEmployee(String firstName, String lastName, double salary) {
        super(firstName, lastName);
        this.dailyRate = salary;
    }

    public ContractEmployee() {
        super();
        this.dailyRate = 0;
    }

    public double getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(double dailyRate) {
        this.dailyRate = dailyRate;
    }

    public String toString() {
        return "Contract Employee:\n" + super.toString() + ", Daily Rate: " + NumberFormat.getCurrencyInstance().format(dailyRate);
    }

    @Override
    public double calculateHourlyRate() {
        return dailyRate/DEFAULT_WORK_HOURS_PER_DAY;
    }

    @Override
    public double getPay() {
        return dailyRate;
    }
}
