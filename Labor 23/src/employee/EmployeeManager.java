package employee;

import java.io.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmployeeManager {

    private List<Employee> employeeList = new ArrayList<>();

    public void addEmployee(Employee employee) {
        employeeList.add(employee);
    }

    public int size() {
        return employeeList.size();
    }

    public int getNumberPermanentEmployees() {
        int sum = 0;
        for (Employee employee : employeeList) {
            if (employee instanceof PermanentEmployee) sum++;
        }
        return sum;
    }

    public int getNumberContractEmployees() {
        int sum = 0;
        for (Employee employee : employeeList) {
            if (employee instanceof ContractEmployee) sum++;
        }
        return sum;
    }

    public int getNumberTemporaryEmployees() {
        int sum = 0;
        for (Employee employee : employeeList) {
            if (employee instanceof TemporaryEmployee) sum++;
        }
        return sum;
    }

    public double calculateAverageHourlyRate() {
        double sum = 0;
        for (Employee employee : employeeList) {
            sum += employee.calculateHourlyRate();
        }
        return sum / employeeList.size();
    }

    public void readFromFile(File data, File log) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(data));
        PrintStream ps = new PrintStream(new FileOutputStream(log, true));

        try {
            ps.println(data.getAbsolutePath() + ": " + new Date().toString());

            String input;
            int lines = 1;
            while ((input = br.readLine()) != null) {
                lines++;
                try {
                    addEmployeeRecord(input);
                    ps.println("Adding employee record \"" + input + "\"");    // Logeintrag schreiben
                    ps.flush();
                } catch (IllegalArgumentException e) {
                    ps.println("Error reading line " + lines + ": " + e.getMessage());
                }
            }
        } finally {
            ps.close();
            br.close();
        }
    }

    public void writeToFile(File file) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(file);

        for (Employee employee : employeeList) {
            printWriter.println(String.format("%s:%s:%s:%f", (employee instanceof PermanentEmployee ? "P" : (employee instanceof ContractEmployee ? "C" : "T")), employee.getFirstName(), employee.getLastName(), employee.getPay()));
        }
        printWriter.close();
    }

    private void addEmployeeRecord(String input) {
        String[] splitted = input.split(":");
        if (splitted.length != 4) throw new IllegalArgumentException("String not formatted properly");

        Employee newEmployee;

        try {
            switch (splitted[0].toLowerCase()) {
                case "c":
                    newEmployee = new ContractEmployee(splitted[1], splitted[2], Double.parseDouble(splitted[3]));
                    break;
                case "p":
                    newEmployee = new PermanentEmployee(splitted[1], splitted[2], Double.parseDouble(splitted[3]));
                    break;
                case "t":
                    newEmployee = new TemporaryEmployee(splitted[1], splitted[2], Double.parseDouble(splitted[3]));
                    break;
                default:
                    throw new IllegalArgumentException("Wrong employee type specified: " + splitted[0]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Argument 4 is not a valid number: " + splitted[3]);
        }

        this.addEmployee(newEmployee);

    }

    public static void main(String[] args) {
        EmployeeManager m = new EmployeeManager();
        m.addEmployee(new ContractEmployee("Klaus", "Huber", 96));
        m.addEmployee(new PermanentEmployee("Eva", "Werner", 20000));
        m.addEmployee(new TemporaryEmployee("Hermann", "Schuster", 11));
        m.addEmployee(new TemporaryEmployee("Christine", "Winter", 12));
        try {
            m.readFromFile(new File("C:\\Users\\Peter Sysel\\Programmieren\\laborübungen\\Labor 23\\emplist.txt"), new File("emplog.txt"));
        } catch (IOException e) {
            System.out.println("Fehler beim Lesen der Daten");
        }
        System.out.format("Anzahl der gespeicherten Employees: %3d\n", m.size());
        System.out.format("Anzahl der ContractEmployees:       %3d\n", m.getNumberPermanentEmployees());
        System.out.format("Anzahl der PermanentEmployees:      %3d\n", m.getNumberContractEmployees());
        System.out.format("Anzahl der TemporaryEmployees:      %3d\n", m.getNumberTemporaryEmployees());
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        System.out.format("Durchschnittlicher Stundenlohn: %s\n", nf.format(m.calculateAverageHourlyRate()));
        try {
            File file = new File("dataneu.txt");
            m.writeToFile(file);
        } catch (IOException e) {
            System.out.println("Fehler beim Schreiben der Daten: "+e.getMessage());
        }
    }

}
