package employee;

public interface Payable {
    double calculateHourlyRate();
    double getPay();
    int DEFAULT_WORK_HOURS_PER_DAY = 8;
    int DEFAULT_WORK_DAYS_PER_YEAR = 250;
}
