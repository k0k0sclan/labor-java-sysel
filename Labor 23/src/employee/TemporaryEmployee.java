package employee;

import java.text.NumberFormat;

public class TemporaryEmployee extends Employee {

    private double hourlyRate;


    public TemporaryEmployee(String firstName, String lastName, double salary) {
        super(firstName, lastName);
        this.hourlyRate = salary;
    }

    public TemporaryEmployee() {
        super();
        this.hourlyRate = 0;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public String toString() {
        return "Temponary Employee:\n" + super.toString() + ", Salary: " + NumberFormat.getCurrencyInstance().format(hourlyRate);
    }

    @Override
    public double calculateHourlyRate() {
        return hourlyRate;
    }

    @Override
    public double getPay() {
        return hourlyRate;
    }
}
