package employee;

import java.text.NumberFormat;

public class PermanentEmployee extends Employee {

    private double salary;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public PermanentEmployee(String firstName, String lastName, double salary) {
        super(firstName, lastName);
        this.salary = salary;
    }

    public PermanentEmployee() {
        super();
        this.salary = 0;
    }

    public String toString() {
        return "Permanent Employee:\n" + super.toString() + ", Salary: " + NumberFormat.getCurrencyInstance().format(salary);
    }

    @Override
    public double calculateHourlyRate() {
        return salary/DEFAULT_WORK_DAYS_PER_YEAR/DEFAULT_WORK_HOURS_PER_DAY;
    }

    @Override
    public double getPay() {
        return salary;
    }
}
