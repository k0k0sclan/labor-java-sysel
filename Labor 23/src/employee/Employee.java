package employee;

import java.text.NumberFormat;
import java.util.Random;

public abstract class Employee implements Payable {
    private String firstName;
    private String lastName;


    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Employee() {
        this.firstName = "N.N.";
        this.lastName = "N.N.";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "First Name: " + firstName + ", Last Name: " + lastName;
    }

//    public static void main1(String[] args) {
//        Employee emp1 = new Employee();
//        emp1.setFirstName("Hans");
//        emp1.setLastName("Huber");
//        System.out.println("emp1: " + emp1.getLastName() + " " + emp1.getFirstName());
//        Employee emp2 = new Employee("Barbara", "Schmidt");
//        System.out.println("emp2: " + emp2);
//    }

    public static void main2(String[] args) {
        Employee pe = new PermanentEmployee();
        Employee ce = new ContractEmployee("Hans", "Huber", 100.0);
        Employee te = new TemporaryEmployee("Barbara", "Schmidt", 7.5);
        System.out.println(pe);
        System.out.println(ce);
        System.out.println(te);
    }

    public static void main3(String[] args) {
        Employee[] f = new Employee[3];
        f[0] = new PermanentEmployee("Franz", "Schuster", 15000.0);
        f[1] = new ContractEmployee("Hans", "Huber", 100.0);
        f[2] = new TemporaryEmployee("Barbara", "Schmidt", 7.5);
        String hourlyRate;
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        for (Employee e : f) {
            System.out.print(e);
            hourlyRate = nf.format(e.calculateHourlyRate());
            System.out.println("\nHourly rate: " + hourlyRate);
        }
    }

    public static void main(String[] args) {
        EmployeeManager m = new EmployeeManager();
        m.addEmployee(new ContractEmployee("Klaus", "Huber", 96));
        m.addEmployee(new PermanentEmployee("Eva", "Werner", 20000));
        m.addEmployee(new TemporaryEmployee("Hermann", "Schuster", 11));
        m.addEmployee(new TemporaryEmployee("Christine", "Winter", 12));
        System.out.format("Anzahl der gespeicherten Employees: %3d\n", m.size());
        System.out.format("Anzahl der ContractEmployees:       %3d\n", m.getNumberContractEmployees());
        System.out.format("Anzahl der PermanentEmployees:      %3d\n", m.getNumberPermanentEmployees());
        System.out.format("Anzahl der TemporaryEmployees:      %3d\n", m.getNumberTemporaryEmployees());
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        System.out.format("Durchschnittlicher Stundenlohn:      %s\n", nf.format(m.calculateAverageHourlyRate()));
    }

}
