/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BarPlotTest;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 * @author reio
 */
public class BarPlotTest {
    
    public BarPlotTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of repeat method, of class BarPlot.
     */
    @Test
    public void testRepeat_1() {
        System.out.println("repeat_1");
        String result = BarPlot.BarPlot.repeat('*', 4);
        assertEquals("****", result);
    }
    
    @Test
    public void testRepeat_2() {
        System.out.println("repeat_2");
        String result = BarPlot.BarPlot.repeat(' ', 5);
        assertEquals("     ", result);
    }
    
    @Test
    public void testRepeat_3() {
        System.out.println("repeat_3");
        String result = BarPlot.BarPlot.repeat('a', 0);
        assertEquals("", result);
    }

    /**
     * Test of drawLabel method, of class BarPlot.
     */
    @Test
    public void testDrawLabel_1() {
        System.out.println("drawLabel_1");
        String result = BarPlot.BarPlot.drawLabel("abc", 6);
        assertEquals("abc   ", result);
    }
    
    @Test
    public void testDrawLabel_2() {
        System.out.println("drawLabel_2");
        String result = BarPlot.BarPlot.drawLabel("abcdefghijk", 6);
        assertEquals("abcdef", result);
    }

    /**
     * Test of createBar method, of class BarPlot.
     */
    @Test
    public void testDrawBar_String_int_1() {
        System.out.println("drawBar_1");
        String result = BarPlot.BarPlot.createBar("WS2009", 15);
        assertEquals("WS2009  |###############               |", result);
    }
    
    @Test
    public void testDrawBar_String_int_2() {
        System.out.println("drawBar_2");
        String result = BarPlot.BarPlot.createBar("DiesIstEinTest", 3);
        assertEquals("DiesIstE|###                           |", result);
    }
    
    @Test
    public void testDrawBar_String_int_3() {
        System.out.println("drawBar_3");
        String result = BarPlot.BarPlot.createBar("Informatik", 33);
        assertNull(result);
    }

    /**
     * Test of createBar method, of class BarPlot.
     */
    @Test
    public void testDrawBar_String_double_1() {
        System.out.println("drawBar_4");
        
        String result = BarPlot.BarPlot.createBar("ColdMonths", 0.8);
        assertEquals("ColdMont|########################      |", result);
    }
    
    @Test
    public void testDrawBar_String_double_2() {
        System.out.println("drawBar_5");
        
        String result = BarPlot.BarPlot.createBar("WarmMonths", 0.15);
        assertEquals("WarmMont|#####                         |", result);
    }
    
    @Test
    public void testDrawBar_String_double_3() {
        System.out.println("drawBar_6");
        
        String result = BarPlot.BarPlot.createBar("WarmMonths", 1.15);
        assertNull(result);
    }
}
