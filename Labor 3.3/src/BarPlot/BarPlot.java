package BarPlot;

import java.util.*;

public class BarPlot {

    public static void main(String[] args){
        ArrayList<Bar> bars = new ArrayList<Bar>();

        String label;
        int valInt = 0;
        double valDouble = 0;
        Scanner sc = new Scanner(System.in);
        int exit = 0;

        while (exit == 0) {
            try {
                System.out.print("Enter the label (exit to leave) and the value (0-30 or 0,0-1,0): ");

                label = sc.next();

                if ("exit".equals(label)) {
                    exit = 1;
                }

                else if (sc.hasNextInt()) {
                    valInt = sc.nextInt();
                    if (valInt < 0 || valInt > 30) {
                        throw new IllegalArgumentException("Invalid number range");
                    }
                    Bar bar = new Bar(label,valInt,valDouble, false);
                    bars.add(bar);
                } else if (sc.hasNextDouble()) {
                    valDouble = sc.nextDouble();
                    if (valDouble < 0.0 || valDouble > 1.0) {
                        throw new IllegalArgumentException("Invalid number range");
                    }
                    Bar bar = new Bar(label,valInt,valDouble, true);
                    bars.add(bar);
                    sc.close();
                } else throw new IllegalArgumentException("Invalid value entered.");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                sc.nextLine();
            }
        }
        

        for (Bar b:bars) {
            if (b.isDouble) System.out.println(createBar(b.label, b.valDouble));
            else System.out.println(createBar(b.label, b.valInt));
        }
    }

    /**
     * Liefert einen String aus n mal dem Zeichen c.
     *
     * @param c Zu wiederholendes Zeichen
     * @param n Anzahl
     * @return Generierter String
     */
    public static String repeat(char c, int n) {
        StringBuilder ret = new StringBuilder();
        for (int i = 0; i < n; i++) {
            ret.append(c);
        }
        return ret.toString();
    }

    /**
     * Liefert den übergebenen String auf n Zeichen modifiziert zurück.
     *
     * @param label Zu modifiziernder String
     * @param n     gewünschte Länge
     * @return null, wenn n<0, ansonsten zugeschnitten/verlängerter Inputstring
     */
    public static String drawLabel(String label, int n) {

        if (n < 0 || label == null) {
            System.err.println("Stringmodifikation nicht möglich!");
            System.err.println("label = [" + label + "], n = [" + n + "]");
            return null;
        }

        if (label.length() < n) {
            return label + repeat(' ', n - label.length());
        } else {
            return label.substring(0, n);
        }
    }

    public static String createBar(String label, int value) {
        String labelNew = drawLabel(label, 8);
        StringBuilder bar = new StringBuilder();
        if (labelNew == null || value < 0 || value > 30) {
            return null;
        }

        return drawBar(value, labelNew, bar);
    }

    private static String drawBar(int value, String labelNew, StringBuilder bar) {
        bar.append(labelNew).append("|");
        for (int i = 0; i < value; i++) {
            bar.append("#");
        }
        for (int i = value; i < 30; i++) {
            bar.append(" ");
        }
        bar.append("|");

        return bar.toString();
    }

    public static String createBar(String label, double value) {
        String labelNew = drawLabel(label, 8);
        StringBuilder bar = new StringBuilder();
        if (labelNew == null || value < 0 || value > 1) {
            return null;
        }

        int count = (int) Math.round(30.0 * value);

        return drawBar(count, labelNew, bar);
    }
}
