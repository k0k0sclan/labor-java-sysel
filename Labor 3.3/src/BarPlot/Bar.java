package BarPlot;

public class Bar {
    String label;
    int valInt;
    double valDouble;
    boolean isDouble;

    Bar(String label,int valInt,double valDouble,boolean isDouble) {
        this.label = label;
        this.valInt = valInt;
        this.valDouble = valDouble;
        this.isDouble = isDouble;
    }
}
