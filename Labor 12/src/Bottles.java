public class Bottles {
    public static void main(String[] args) {
        printSong("juice");
    }

    private static void printSong(final String drink) {
        for (int i = 99; i >= 1; i--) {
            String s = i > 1 ? "s" : "";
            System.out.println(i + " bottle" + s + " of " + drink + " on the wall, " + i + " bottle" + s + " of " + drink + ".");
            int j = i - 1;
            String next = j > 1 ? j + " bottles" : j == 1 ? "1 bottle" : "no more bottles";
            System.out.println("Take one down and pass it around, " + next + " of " + drink + " on the wall.");
            System.out.println();
        }
        System.out.print("No more bottles of " + drink + " on the wall, ");
        System.out.println("no more bottles of " + drink + ".");
        System.out.println("Go to the store and buy some more, 99 bottles of " + drink + " on the wall.");
    }
}
