package dualLinkedList;

@SuppressWarnings("ALL")
public class DualLinkedList {

    private int size;
    private ListNode head;
    private ListNode tail;

    public DualLinkedList() {
    }


    public boolean isEmpty() {
        return size == 0;
    }

    public void add(Object data) {

        ListNode neu = new ListNode(data);
        if (isEmpty()) {
            head = neu;
            tail = neu;
        } else {
            tail.setNext(neu);
            neu.setPrevious(tail);
            tail = neu;
        }
        size++;
    }

    public void add(Object data, int index) {

        if (index < 0 || index > size) {
            System.err.println("Index illegal: " + index);
            return;
        }
        if (isEmpty()) {
            add(data);
        } else if (index == 0) {
            ListNode neu = new ListNode(data);
            neu.setNext(head);
            neu.setPrevious(head);
            head = neu;
            size++;
        } else {
            ListNode akt = head;
            for (int i = 0; i < index - 1; i++) {
                akt = akt.getNext();
            }
            //akt ist Knoten direkt vor der gewünschten Position
            ListNode neu = new ListNode(data);
            neu.setNext(akt.getNext());
            neu.setPrevious(akt);
            akt.getNext().setPrevious(neu);
            akt.setNext(neu);
            if (neu.getNext() == null) {
                tail = neu;
            }
            size++;
        }

    }

    public Object get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }

        ListNode akt = head;

        for (int i = 0; i < index; i++) {
            akt = akt.getNext();
        }

        return akt.getData();
    }

    public Object del(int index) {

        Object data;

        if (index >= size || index < 0) {
            return null;
        }


        if (index == 0) {
            data = head.getData();
            if (size>1) {
                head.getNext().setPrevious(null);
            }
            head = head.getNext();
            size--;
            return data;
        }

        ListNode akt = head;
        for (int i = 0; i < index - 1; i++) {
            akt = akt.getNext();
        }
        if (index == size - 1) {
            data = tail.getData();
            tail.setPrevious(null);
            tail = akt;
            tail.setNext(null);
            size--;
            return data;
        } else {
            data = akt.getNext().getData();
            akt.getNext().getNext().setPrevious(akt);
            akt.setNext(akt.getNext().getNext());
            size--;
            return data;
        }
    }

    public int size() {
        return size;
    }

    public void delAll(Object data) {
        if (data == null) {
            return;
        }

        ListNode akt = head;

        for (int i = 0; akt!=null ; akt=akt.getNext()) {
            if (akt.getData() == data) {
                del(i);
            }
            else i++;
        }

    }


    @Override
    public String toString() {

        if (!isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder("[");
            ListNode akt = head;

            while (akt.getNext() != null) {
                stringBuilder.append(akt.getData()).append(", ");
                akt = akt.getNext();
            }
            stringBuilder.append(akt.getData()).append("]");

            return stringBuilder.toString();

        } else {
            return "[]";
        }
    }

}
