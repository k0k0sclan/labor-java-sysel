package dualLinkedList;

public class ListNode {

    private ListNode next;
    private ListNode previous;

    public ListNode getPrevious() {
        return previous;
    }

    public ListNode(ListNode next, ListNode previous, Object data) {
        this.next = next;
        this.previous = previous;
        this.data = data;
    }

    public void setPrevious(ListNode previous) {
        this.previous = previous;
    }

    private Object data;

    public ListNode(ListNode next, Object data) {
        this.next = next;
        this.data = data;
    }

    public ListNode() {
    }

    public ListNode(Object data) {
        this(null, data);
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
