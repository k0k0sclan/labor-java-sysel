package linkedList;

public class Main {

    public static void main(String[] args) {

        LinkedList list = new LinkedList();
        list.add('A');
        list.add('B');
        list.add('C');
        list.add('D');
        list.add("X", 2);
        list.add("Z", 5);

        System.out.println(list);

    }

}
