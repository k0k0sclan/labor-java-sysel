package linkedList;

public class LinkedList {

    private int size;
    private ListNode head;
    private ListNode tail;

    public LinkedList() {
    }


    public boolean isEmpty() {
        return size == 0;
    }

    public void add(Object data) {

        ListNode neu = new ListNode(data);
        if (isEmpty()) {
            head = neu;
            tail = neu;
        } else {
            tail.setNext(neu);
            tail = neu;
        }
        size++;
    }

    public void add(Object data, int index) {

        if (index < 0 || index > size) {
            System.err.println("Index illegal: " + index);
            return;
        }
        if (isEmpty()) {
            add(data);
        } else if (index == 0) {
            ListNode neu = new ListNode(data);
            neu.setNext(head);
            head = neu;
            size++;
        } else {
            ListNode akt = head;
            for (int i = 0; i < index - 1; i++) {
                akt = akt.getNext();
            }
            //akt ist Knoten direkt vor der gewünschten Position
            ListNode neu = new ListNode(data);
            neu.setNext(akt.getNext());
            akt.setNext(neu);
            if (neu.getNext() == null) {
                tail = neu;
            }
            size++;
        }

    }

    public Object get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }

        ListNode akt = head;

        for (int i = 0; i < index; i++) {
            akt = akt.getNext();
        }

        return akt.getData();
    }

    public Object del(int index) {

        Object data;

        if (index >= size || index < 0) {
            return null;
        }


        if (index == 0) {
            data = head.getData();
            head = head.getNext();
            size--;
            return data;
        }

        ListNode akt = head;
        for (int i = 0; i < index - 1; i++) {
            akt = akt.getNext();
        }
        if (index == size - 1) {
            data = tail.getData();
            tail = akt;
            tail.setNext(null);
            size--;
            return data;
        } else {
            data = akt.getNext().getData();
            akt.setNext(akt.getNext().getNext());
            size--;
            return data;
        }
    }

    public int size() {
        return size;
    }


    @Override
    public String toString() {

        if (!isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder("[");
            ListNode akt = head;

            while (akt.getNext() != null) {
                stringBuilder.append(akt.getData()).append(", ");
                akt = akt.getNext();
            }
            stringBuilder.append(akt.getData()).append("]");

            return stringBuilder.toString();

        } else {
            return "[]";
        }
    }

}
