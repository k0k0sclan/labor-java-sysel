public class Konto {
    private String name;
    private double kontostand;
    private final boolean girokonto;
    private int transaktionen;

    Konto(String name, double kontostand, boolean girokonto) {
        this.name = name;
        this.kontostand = kontostand;
        this.girokonto = girokonto;
        transaktionen = 0;
    }

    static void umbuchen(Konto k1, Konto k2, double wert) {
        if (k1.abheben(wert)) {
            k2.einzahlen(wert);
        }
    }

    void einzahlen(double wert) {
        if (wert > 0) {
            kontostand += wert;
            transaktionen++;
        }
    }

    boolean abheben(double wert) {
        if (!girokonto && wert > kontostand) {
            System.out.println("Konto " + name + " nicht gedeckt!");
        } else if (wert > 0) {
            kontostand -= wert;
            transaktionen++;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format(
                "\nKontoname: %s\n" +
                        "Stand: %f\n" +
                        "Transaktionen: %d\n" +
                        "Girokonto: %s\n",
                this.name, this.kontostand, this.transaktionen, girokonto ? "ja" : "nein");
    }


    public static void main(String []args) {
        Konto k1 = new Konto("Giro 1", 50.0, true);
        System.out.println(k1);
        k1.einzahlen(100);
        k1.abheben(200);
        System.out.println(k1);
        Konto k2 = new Konto("Spar 1", 50.0, false);
        System.out.println(k2);
        k2.einzahlen(100);
        k2.abheben(200);
        System.out.println(k2);
        Konto.umbuchen(k1, k2, 33);
        System.out.println(k1);
        System.out.println(k2);
    }

}
