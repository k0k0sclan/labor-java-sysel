

import java.util.Objects;

import linkedList.LinkedList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class LinkedListTest {

    LinkedList list;

    @Before
    public void setUp() {
        list = new LinkedList();
        list.add("Eins");
        list.add("Zwei");
        list.add("Drei");
    }

    /**
     * Vergleicht zwei linkedList.LinkedList Objekte auf equals-Gleichheit.
     *
     * @param l1 erste linkedList.LinkedList
     * @param l2 zweite linkedList.LinkedList
     * @return true wenn komplett gleich, sonst false
     */
    private static boolean equal(LinkedList l1, LinkedList l2) {
        if (l1 == null && l2 == null) {
            return true;
        }
        if (l1 == null || l2 == null) {
            return false;
        }
        if (l1.size() != l2.size()) {
            return false;
        }
        for (int i = 0; i < l1.size(); i++) {
            boolean equal = Objects.equals(l1.get(i), l2.get(i));
            if (!equal) {
                return false;
            }
        }
        return true;
    }

    /**
     * Test of size method, of class linkedList.LinkedList.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        assertEquals(3, list.size());
        list = new LinkedList();
        assertEquals(0, list.size());
        list.add(3);
        assertEquals(1, list.size());
        list.add(3);
        assertEquals(2, list.size());
    }

    /**
     * Test of delNode method, of class linkedList.LinkedList.
     */
    @Test
    public void testDel() {
        System.out.println("del");
        LinkedList tmp = new LinkedList();
//    assertNull(tmp.del(0));
//    assertNull(list.del(-5));
//    assertNull(list.del(3));
        assertEquals("Eins", list.del(0));

        LinkedList ref = new LinkedList();
        ref.add("Zwei");
        ref.add("Drei");
        assertTrue(equal(ref, list));

        setUp();
        assertEquals("Zwei", list.del(1));

        ref = new LinkedList();
        ref.add("Eins");
        ref.add("Drei");
        assertTrue(equal(ref, list));

        setUp();
        assertEquals("Drei", list.del(2));

        ref = new LinkedList();
        ref.add("Eins");
        ref.add("Zwei");
        assertTrue(equal(ref, list));

        setUp();
        tmp.add("Eins");
        tmp.add("Zwei");
        tmp.add("Drei");
        tmp.add("Vier");
        tmp.add(null);
        assertNull(tmp.del(4));
        assertEquals("Vier", tmp.del(3));

        setUp();
        assertTrue(equal(tmp, list));

    }

    /**
     * Test of toString method, of class linkedList.LinkedList.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        assertEquals("[Eins, Zwei, Drei]", list.toString());
        list = new LinkedList();
        assertEquals("[]", list.toString());
    }

    /**
     * Test of add method, of class linkedList.LinkedList.
     */
    @Test
    public void testAdd_Object_int() {
        System.out.println("add");
        String toAdd = "new";
        list.add(toAdd, 0);
        assertEquals(toAdd, list.get(0));
        assertEquals("Eins", list.get(1));

        setUp();
        list.add(toAdd, 1);
        assertEquals("Eins", list.get(0));
        assertEquals(toAdd, list.get(1));
        assertEquals("Zwei", list.get(2));

        setUp();
        list.add(toAdd, 2);
        assertEquals("Eins", list.get(0));
        assertEquals("Zwei", list.get(1));
        assertEquals(toAdd, list.get(2));
        assertEquals("Drei", list.get(3));

    }

    /**
     * Test of get method, of class linkedList.LinkedList.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        assertEquals("Eins", list.get(0));
        assertEquals("Zwei", list.get(1));
        assertEquals("Drei", list.get(2));
    }
}
