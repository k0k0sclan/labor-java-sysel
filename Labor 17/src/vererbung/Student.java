package vererbung;

import java.util.ArrayList;
import java.util.LinkedList;

public class Student extends Person {

    int matrikelnummer;

    public int getMatrikelnummer() {
        return matrikelnummer;
    }

    public void setMatrikelnummer(int matrikelnummer) {
        this.matrikelnummer = matrikelnummer;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + matrikelnummer;
    }

    public static Student[] search(Student[] f, String pattern) {
        ArrayList<Student> results = new ArrayList<Student>();
        for (Student student : f) {
            if (student.getName().contains(pattern)) results.add(student);
        }
        return results.toArray(new Student[0]);
    }


    public Student(String name, String vorname, int matrikelnummer) {
        super(name, vorname);
        this.matrikelnummer = matrikelnummer;
    }

    public static void main(String[] args) {

        String[] vn = {"Alfred", "Bernd", "Carola", "Dieter", "Erich", "Gerda", "Hans", "Jochen", "Karin", "Maike"};
        String[] nn = {"Adler", "Baron", "Claus", "Dimov", "Eliot", "Gatti", "Heine", "Jahn", "Kozak", "Miller"};
        int[] mn = {12345, 45671, 23456, 11111, 45667, 98712, 23456, 65123, 12634, 22222};
        Student[] students = new Student[vn.length];

        for (int i = 0; i < vn.length; i++) {
            students[i] = new Student(nn[i], vn[i], mn[i]);
        }

        for (Student student : students) {
            System.out.println(String.format("%10s | %10s | %d",student.getVorname(),student.getName(),student.getMatrikelnummer()));
        }

        System.out.println("Suchergebnisse: ");

        for (Student student :
                Student.search(students, "ar")) {
            System.out.println(student);
        }

    }

}

