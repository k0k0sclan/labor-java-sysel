package vererbung;

public class Person {

    private String name;
    private String vorname;

    public Person(String name, String vorname) {
        this.name = name;
        this.vorname = vorname;
    }

    public Person() {
        this("Mustermann", "Max");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    @Override
    public String toString() {
        return this.getName() + " " + this.getVorname();
    }

    public static void main(String[] args) {
        Person p1;
        p1 = new Person();
        p1.setName("Schoettle");
        p1.setVorname("Lothar");
        System.out.println("Vorname : " + p1.getVorname());
        System.out.println("Name : " + p1.getName());
        Person p2 = new Person("Klammer", "Franz");
        System.out.println(p2);    // Ausgabe: "Klammer Franz"

    }
}