package vererbung;

public class Konto {

    private double kontostand;
    private Person inhaber;

    public Konto(Person inhaber, double kontostand) {
        this.kontostand = kontostand;
        this.inhaber = inhaber;
    }

    public Konto(String name, String vorname, double kontostand) {
        this(new Person(name,vorname),kontostand);
    }

    public static void vergleich(Konto konto1, Konto konto2) {
        if (konto1.kontostand > konto2.kontostand) {
            System.out.println(konto1.inhaber.getName()+" hat mehr auf dem Konto als "+konto2.inhaber.getName());
        }
        else if (konto1.kontostand < konto2.kontostand) {
            System.out.println(konto2.inhaber.getName()+" hat mehr auf dem Konto als "+konto1.inhaber.getName());
        }
        else if (konto1.kontostand == konto2.kontostand) {
            System.out.println(konto1.inhaber.getName()+" und " + konto2.inhaber.getName()+ " haben den gleichen Kontostand.");
        }
    }

    public static void main(String[] args) {
        Konto konto1 = new Konto("Schmidt", "Hans", 500.0);
        Konto konto2 = new Konto("Krause", "Peter", 1500.0);
        Konto konto3 = new Konto("Bauer", "Harald", 500.0);
        Konto.vergleich(konto1, konto2);
        Konto.vergleich(konto2, konto3);
        Konto.vergleich(konto1, konto3);
    }

}
