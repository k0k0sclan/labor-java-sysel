package vererbung;

public class Fahrrad {

    private String fahrer;
    private String farbe;
    private int richtung;
    private double geschwindigkeit;

    public Fahrrad(String fahrer, String farbe) {
        this.fahrer = fahrer;
        this.farbe = farbe;
    }

    public Fahrrad(String farbe) {
        this("nicht vorhanden", farbe);
    }

    public Fahrrad() {
        this("nicht vorhanden", "unlackiert");
    }


    public void lenken(int deltaR) {
        if(Math.abs(deltaR+richtung)<45) richtung += deltaR;
    }

    public void beschleunigen(double a, double second) {
        if (second < 0) throw new IllegalArgumentException("Time can't run backwards!");
        geschwindigkeit += a * second;
        if (geschwindigkeit<0) geschwindigkeit = 0;
    }

    public double getKmh() {
        return geschwindigkeit * 3.6;
    }

    @Override
    public String toString() {
        return "Fahrer: " + fahrer +
                ", Farbe: " + farbe +
                ", Richtung: " + richtung +
                ", Geschwindigkeit: " + getKmh();
    }

    public static void main(String[] args) {
        Fahrrad r = new Fahrrad("Strampler", "blau");
        r.lenken(10);
        r.beschleunigen(0.3, 9.8);  // v = v + 0.3*9.8
        System.out.println(r);}
    }
