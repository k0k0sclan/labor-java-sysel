package vererbung;

public class Stadtrad extends Fahrrad {

    boolean licht;

    public Stadtrad(String fahrer, String farbe, boolean licht) {
        super(fahrer, farbe);
        this.licht = licht;
    }

    public boolean isLicht() {
        return licht;
    }

    public void setLicht(boolean licht) {
        this.licht = licht;
    }

    @Override
    public String toString() {
        return super.toString()+", Licht " + (isLicht()?"an":"aus");
    }

    public static void main(String[] args) {
        Fahrrad r = new Stadtrad("Thomas", "schwarz", false);
        r.lenken(10);
        r.beschleunigen(0.3, 9.8);
        ((Stadtrad) r).setLicht(true);
        System.out.println(r);
    }

}
