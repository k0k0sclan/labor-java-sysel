package datum;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Datum {

    // Konstante fuer die Formatierung der Ausgabe
    public final static int SHORT = 0;
    public final static int NORMAL = 1;
    public final static int LONG = 2;
    public final static int US = 3;

    private int day;
    private int month;
    private int year;


    /**
     * ∗Erzeugt eine Datumsinstanz mit dem aktuellen Systemdatum
     */
    public Datum() {
        Calendar calendar = Calendar.getInstance();
        this.day = calendar.get(GregorianCalendar.DAY_OF_MONTH);
        this.month = calendar.get(GregorianCalendar.MONTH);
        this.year = calendar.get(GregorianCalendar.YEAR);
    }

    /**
     * ∗Erzeugt eine Datumsinstanz, die t Tage nach dem 1.1.1900 liegt.∗Bei negativem t wird der 31.12.1899 gespeichert. ∗@param t Tage seit dem 1.1.1900
     */
    public Datum(int t) {
        this.day = 1;
        this.month = 1;
        this.year = 1900;
        addiereTage(t < 0 ? -1 : t);
    }

    /**
     * ∗Erzeugt eine Datumsinstanz mit den gegebenen Werten.∗Bei einem ungueltigen Datum wird der 31.12.1899 gespeichert.∗@param tag der Tag 1-31 ( abhaengig vom Monat)∗@param monat das Monat, 1 -  12∗@param jahr das Jahr, 1900 - 3000
     */

    public Datum(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
        if(!this.isValid()){
            this.day = 31;
            this.month = 12;
            this.year = 1899;
        }
    }

    public Datum(Datum datum) {
        this(datum.day, datum.month, datum.year);
    }

    public Datum(String datum) {
        var match = Pattern.compile("^(\\d+)\\.(\\d+)\\.(\\d+)$", Pattern.MULTILINE).matcher(datum);
        if (!match.matches()) throw new IllegalArgumentException("Falsches Datumsformat");
        this.day = Integer.parseInt(match.group(1));
        this.month = Integer.parseInt(match.group(2));
        this.year = Integer.parseInt(match.group(3));
        if (!this.isValid()) throw new IllegalArgumentException("Ungültiges Datum");
    }

    /**
     * ∗Prüft, ob das Datum gueltig ist.∗@return true, wenn gueltig, false, wenn ungueltig
     */
    public boolean isValid() {

        if (this.compareTo(new Datum(0)) < 0) return false;

        if (this.month <= 0 || this.month > 12) {                                         //Monatuberprufung
            return false;
        }
        return day <= getMonthDuration() && day >= 1;
    }

    public int getMonthDuration() {
        switch (this.month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                if (Datum.schaltjahr(this.year)) {
                    return 29;
                } else {
                    return 28;
                }
        }
        throw new IllegalStateException(String.format("Month %d does not exist", month));
    }

    /**
     * ∗Liefert das Jahr dieses Datums∗@return das Jahr
     */
    public int getJahr() {
        return this.year;
    }

    /**
     * ∗Liefert das Monat dieses Datums∗@return das Monat, einen Wert im Bereich 1-12
     */
    public int getMonat() {
        return this.month;
    }

    /**
     * ∗Liefert den Namen des Monats∗@return den Monatsnamen (Jaenner, Februar, Maerz,∗April, Mai, Juni, Juli, August, September,∗Oktober, November, Dezember
     */
    public String getMonatByString() {

        if (month < 1 || month > 12) throw new IllegalStateException(String.format("Monat %d existiert nicht", month));
        String[] months = {"Jaenner", "Februar", "Maerz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"};

        return months[month-1];

    }

    /**
     * ∗Liefert den Tag dieses Datums∗@return den this.day im Monat (1 - 31)
     */
    public int getTag() {
        return day;
    }

    /**
     * ∗Erhoeht (vermindert) das gespeicherte Datum um t Tage.∗Liegt nach dieser Operation das Datum vor dem 1.1.1900,∗so wird der 31.12.1899 gespeichert.∗@param t Anzahl der Tage, um die dieses Datum erhoeht∗(t > 0) bzw. vermindert (t < 0) wird
     */
    public void addiereTage(int t) {

        if (t > 0) {
            int monthMax = getMonthDuration();
            for (int i = 1; i < t + 1; i++) {
                if (day == monthMax) {
                    if (month == 12) {
                        year++;
                        month = 0;
                    }
                    month++;
                    day = 1;
                    monthMax = getMonthDuration();
                } else day++;
            }
        } else {
            for (int i = 1; i < t * (-1) + 1; i++) {
                if (day == 1) {
                    if (month == 1) {
                        year--;
                        month = 13;
                    }
                    month--;
                    day = getMonthDuration();
                } else day--;
            }


        }

    }

    /**
     * ∗Liefert die Nummer des Wochentages. ∗@return Nummer des Wochentages im Bereich von∗0(Montag) bis 6(Sonntag) bzw. - 1  bei ungueltigem Datum
     */
    public int wochentagNummer() {

        int d = this.day;
        int m = this.month;
        int y = this.year;

        if (!isValid()) return -1;
        int[] t = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
        if (m < 3) y--;
        return (y + y / 4 - y / 100 + y / 400 + t[m - 1] + d - 1) % 7;
    }

    /**
     * ∗Liefert den Wochentag als String∗@return Wochentag als String∗Montag, Dienstag, Mittwoch, Donnerstag,∗Freitag Samstag, Sonntag bzw. <code>null</code>∗bei einem ungueltigen Datum
     */
    public String wochentag() {
        if (!isValid()) return null;
        String[] wochentage = {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"};
        return wochentage[wochentagNummer()];
    }

    /**
     * Liefert die zwischen zwei Datumsangaben vergangenen
     * Tage.
     *
     * @param d1 erstes Datum
     * @param d2 zweites Datum
     * @return Tage zwischen d1 und d2, postiv wenn d2 nach d1
     * liegt, sonst negativ
     */
    public static int tageZwischen(Datum d1, Datum d2) {

        int counter;

        if (d1.compareTo(d2) > 0) return Datum.tageZwischen(new Datum(d2), d1) * (-1);
        else if (d1.equals(d2)) return 0;

        Datum neu1 = new Datum(d1);
        for (counter = 0; !neu1.equals(d2); neu1.addiereTage(1)) {
            counter++;
        }
        return counter;
    }

    /**
     * ∗Vergleicht dieses Datum mit d∗@param d Datum, mit dem dieses Datum verglichen wird∗@return negativ, wenn d spaeter liegt, positiv, wenn d frueher∗liegt und 0 bei gleichem Datum
     */
    public int compareTo(Datum d) {
        if (year < d.year) return -1;
        if (year > d.year) return 1;
        if (month < d.month) return -1;
        if (month > d.month) return 1;
        if (day < d.day) return -1;
        if (day > d.day) return 1;
        return 0;
    }

    /**
     * ∗Prueft auf Schaltjahr∗@param jahr die zu pruefende Jahreszahl∗@return true, wenn <code>jahr</code> ein Schaltjahr∗ist, <code>false</code> sonst
     */
    public static boolean schaltjahr(int jahr) {
        if (jahr % 100 == 0) return jahr % 400 == 0;
        else return jahr % 4 == 0;
    }

    /**
     * ∗Liefert eine Stringdarstellung in der Form∗<code>tt.mm.jjjj</code>∗@override∗@return Stringdarstellung in der Form <code>tt.mm.jjjj</code>∗bzw. <code>ungueltig</code> bei ungueltigem Datum.
     */
    public String toString() {
        return String.format("%02d.%02d.%d", day, month, year);
    }

    /**
     * ∗Liefert eine Stringdarstellung unterschiedlichen Formats∗@param format Moegliche Werte sind:
     * ∗<code>Datum.SHORT, Datum.NORMAL, Datum.LONG, Datum.US</code>
     * ∗@return Datum
     * ∗im Format <code>dd.mm.yy</code> bei <code>Datum.SHORT</code>,
     * ∗im Format <code>dd.monat jjjj, wochentag (Monat ausgeschrieben)</code>
     * ∗bei <code>Datum.LONG</code>,
     * ∗im Format <code>jjjj/tt/mm</code> bei <code>Datum.US</code>
     */
    public String toString(int format) {

        switch (format) {
            case Datum.SHORT:
                return String.format("%d.%02d.%02d", day, month, year % 100);
            case Datum.NORMAL:
                return String.format("%02d.%02d.%d", day, month, year);
            case Datum.LONG:
                return String.format("%d.%s %d, %s", day, this.getMonatByString(), year, this.wochentag());
            case Datum.US:
                return String.format("%d/%02d/%02d", year, day, month);
            default:
                throw new IllegalArgumentException(format + " not a valid format number");
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Datum datum = (Datum) o;

        if (day != datum.day) return false;
        if (month != datum.month) return false;
        return year == datum.year;
    }

    @Override
    public int hashCode() {
        int result = day;
        result = 31 * result + month;
        result = 31 * result + year;
        return result;
    }
}
