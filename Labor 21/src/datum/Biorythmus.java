package datum;

import java.util.Scanner;

public class Biorythmus {

    private double physisch;
    private double emotionell;
    private double geistig;

    public static void main(String[] args) {

        Biorythmus[] biorythmuses = new Biorythmus[30];
        Scanner scanner = new Scanner(System.in);
        Datum geburtsDatum = null;
        Datum heute = null;
        try {
            System.out.print("Geburtsdatum eingeben -> ");
            geburtsDatum = new Datum(scanner.nextLine());
            System.out.print("Datum für Biorythmus eingeben -> ");
            heute = new Datum(scanner.nextLine());
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        for (int i = 0; i < 30; i++) {
            biorythmuses[i] = getBiorythmus(geburtsDatum, heute);
            heute.addiereTage(1);
        }
        System.out.println("Tag| physi | emoti | geist");
        for (int i = 0; i < biorythmuses.length; i++) {
            System.out.printf("%2d | %5.2f | %5.2f | %5.2f\n", i + 1, biorythmuses[i].physisch, biorythmuses[i].emotionell, biorythmuses[i].geistig);
        }
    }

    static Biorythmus getBiorythmus(Datum gebDatum, Datum datum) {
        Biorythmus biorythmus = new Biorythmus();
        int tageBisher = Datum.tageZwischen(gebDatum, datum);
        biorythmus.physisch = Math.sin((2 * (tageBisher % 23) * Math.PI) / 23);
        biorythmus.emotionell = Math.sin((2 * (tageBisher % 28) * Math.PI) / 28);
        biorythmus.geistig = Math.sin((2 * (tageBisher % 33) * Math.PI) / 33);
        return biorythmus;
    }

}
