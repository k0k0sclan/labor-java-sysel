public class Potion {

    private Ingredient[] ingredients;
    private int stirs;

    Potion(Ingredient ...ingredients) {
        if (ingredients.length != 3) System.exit(1);

        this.ingredients = ingredients;
        this.stirs = 0;
    }

    void stir() {
        this.stirs++;
    }

    boolean ready() {
        return this.stirs>=5;
    }

    int power() {
        int power = this.ingredients[0].power() + this.ingredients[1].power() + this.ingredients[2].power();
        return this.ready() ? power * 2 : power;
    }

    @Override
    public String toString() {
        return String.format("\nZutaten: %s, %s und %s.\n" +
                             "Zaubertrank fertig: %s\n" +
                             "Stärke des Tranks: %d\n",
                              this.ingredients[0],this.ingredients[1],this.ingredients[2],
                              this.ready()?"Ja":"Nein",
                              this.power());
    }

}
