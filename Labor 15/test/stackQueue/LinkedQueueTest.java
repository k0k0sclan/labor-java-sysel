/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stackQueue;

import org.junit.*;

import static org.junit.Assert.*;

/**
 *
 * @author maus
 */
public class LinkedQueueTest {
    private LinkedQueue queue1, queue2;

    public LinkedQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        queue1 = new LinkedQueue();
        for (int i = 0; i < 6; i++) {
            queue1.add(i);
        }
        queue2 = new LinkedQueue();
        queue2.add("Eins");
        queue2.add("Zwei");
        queue2.add("Drei");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class LinkedQueue.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        assertEquals(queue1.size() , 6);
        assertEquals(queue2.size() , 3);
        queue1 = new LinkedQueue();
        assertEquals(queue1.size() , 0);
        queue1.add(1);
        queue1.add(1);
        queue1.add(1);
        assertEquals(queue1.size() , 3);
        queue1.get();
        assertEquals(queue1.size() , 2);
    }

    /**
     * Test of isEmpty method, of class LinkedQueue.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        assertFalse(queue1.isEmpty());
        assertFalse(queue2.isEmpty());
        queue1 = new LinkedQueue();
        assertTrue(queue1.isEmpty());
        for (int i = 0; i < 3; i++) {
            queue2.get();
        }
        assertTrue(queue2.isEmpty());
    }

    /**
     * Test of add method, of class LinkedQueue.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        for (int i = 0; i < 3; i++) {
            assertEquals(queue1.get(), i);
        }
        for(int i = 0; i < 3; i++) {
            queue1.add(i);
        }
        for (int i = 0; i < 3; i++) {
            assertEquals(queue1.get(), i+3);
        }
        for (int i = 0; i < 3; i++) {
            assertEquals(queue1.get(), i);
        }
        assertNull(queue1.get());
        assertEquals(queue2.get(), "Eins");
        queue2.add("Vier");
        assertEquals(queue2.get(), "Zwei");
        assertEquals(queue2.get(), "Drei");
        assertEquals(queue2.get(), "Vier");
        assertNull(queue2.get());
    }

    /**
     * Test of get method, of class LinkedQueue.
     */
    @Test
    public void testGet() {
        System.out.println("get");

    }

    /**
     * Test of getNth method, of class LinkedQueue.
     */
    @Test
    public void testGetNth() {
        System.out.println("getNth");
        assertEquals(queue1.getNth(4), 4);
        assertEquals(queue1.getNth(4), 5);
        assertNull(queue1.getNth(4));
        queue1.add(5);
        assertEquals(queue1.getNth(4),5);
        assertNull(queue1.getNth(-3));
        assertEquals(queue1.getNth(0), 0);
        assertEquals(queue1.getNth(0), 1);
        assertEquals(queue2.getNth(1), "Zwei");
        queue2.add("Vier");
        assertEquals(queue2.getNth(2), "Vier");
        assertEquals(queue2.getNth(1), "Drei");
    }

    /**
     * Test of element method, of class LinkedQueue.
     */
    @Test
    public void testElement() {
        System.out.println("element");
        for (int i = 0; i < 6; i++) {
            assertEquals(queue1.element(), 0);
        }
        for (int i = 0; i < 6; i++) {
            assertEquals(queue1.element(), i);
            queue1.get();
        }
        assertEquals(queue2.element(), "Eins");
        queue2.get();
        assertEquals(queue2.element(), "Zwei");
        queue2.getNth(1);
        assertEquals(queue2.element(), "Zwei");
    }

    /**
     * Test of toString method, of class LinkedQueue.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        assertEquals(queue1.toString(), "[0, 1, 2, 3, 4, 5]");
        assertEquals(queue2.toString(), "[Eins, Zwei, Drei]");
        queue2.add("4");
        assertEquals(queue2.toString(), "[Eins, Zwei, Drei, 4]");
        queue2.getNth(1);
        assertEquals(queue2.toString(), "[Eins, Drei, 4]");
        queue2.getNth(2);
        assertEquals(queue2.toString(), "[Eins, Drei]");
    }

}
