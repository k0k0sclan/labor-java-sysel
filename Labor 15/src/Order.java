import stackQueue.LinkedQueue;

public class Order {

    String customer;
    int quantity;

    public Order(String customer, int quantity) {
        this.customer = customer;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Customer: " + customer + "\n" +
                "Quantity: " + quantity + '\n';
    }

    public static void main(String[] args) {

        LinkedQueue queue = new LinkedQueue();

        for (int i = 0; i < 10; i++) {
            queue.add(new Order("Kammi",(int)Math.floor(Math.random()*30+20)));
        }
        System.out.println("queue = " + queue.size());
        dumpQueue(queue);

    }

    private static void dumpQueue(LinkedQueue queue) {
        System.out.println("********* QUEUE DUMP *********");
        System.out.println(queue);
    }

}
