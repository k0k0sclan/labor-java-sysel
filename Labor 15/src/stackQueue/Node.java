package stackQueue;

public class Node {

    private Object elem;

    public Object getElem() {
        return elem;
    }

    public void setElem(Object elem) {
        this.elem = elem;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    private Node next;

    public Node() {
    }

    public Node(Object elem) {
        this.elem = elem;
    }

    @Override
    public String toString() {
        return elem.toString();
    }
}
