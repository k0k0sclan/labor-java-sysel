package stackQueue;

public class LinkedStack {

    private Node tos;
    private int size;

    public LinkedStack() {
        this.tos = null;
        this.size = 0;
    }

    public void push(Object elem) {
        Node node = new Node(elem);
        if (isEmpty()) tos = node;
        else {
            node.setNext(tos);
            tos = node;
        }
        size++;
    }

    public Object pop() {
        Object elem = isEmpty()?null:tos.getElem();
        tos = !isEmpty() ? tos.getNext() : null;
        size--;
        return elem;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public Object element() {
        return tos!=null?tos.getElem(): null;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (Node node = tos;node!=null;node = node.getNext()) {
            out.append(node.getElem()).append("\n");
        }
        return out.toString();
    }

}
