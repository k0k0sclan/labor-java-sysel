package stackQueue;

public class LinkedQueue {

    private Node head;
    private Node tail;
    private int size;

    public LinkedQueue() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public boolean add(Object elem) {
        Node node = new Node(elem);
        if (isEmpty()) {
            head = node;
            tail = node;
        } else {
            tail.setNext(node);
            tail = node;
        }
        size++;
        return true;
    }

    public Object get() {
        Object elem = !isEmpty() ? head.getElem() : null;
        head = !isEmpty() ? head.getNext() : null;
        size--;
        return elem;
    }

    public Object getNth(int index) {

        if (isEmpty()||index>=size || index<0) return null;

        if (index == 0) {
            Object data = head.getElem();
            head = head.getNext();
            size--;
            return data;
        }

        Node akt = head;

        for (int i = 0; i < index-1; i++) {
            akt=akt.getNext();
        }

        Object data = akt.getNext().getElem();

        akt.setNext(akt.getNext().getNext());

        if (akt.getNext()==null) tail = akt;

        size--;
        return data;

    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public Object element() {
        return head!=null?head.getElem(): null;
    }

    @Override
    public String toString() {

        if(isEmpty()) return "[]";

        StringBuilder out = new StringBuilder();
        int i = 1;
        for (Node akt = head;akt!=null;akt = akt.getNext()) {
            out.append("*** Element ").append(i++).append("\n").append(akt).append("\n").append("*********");
        }
        return out.append("]").toString();
    }

}
