import stackQueue.LinkedStack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        LinkedStack stack = new LinkedStack();
        Scanner scanner = null;

        try {
            scanner = new Scanner(new File("input.txt"));
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(0);
        }

        while (scanner.hasNextLine()) {
            stack.push(scanner.nextLine());
        }

        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }

    }

}
